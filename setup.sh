#!/bin/bash

echo "Fetching project submodules... "
git submodule update --init --recursive

echo "Initializing BOSP..."
cd bosp && make bootstrap

