
MANGO_ROOT ?= /opt/mango
BOSP_PATH  ?= $(MANGO_ROOT)/bosp
BUILD_TYPE ?= Release
CPUS       ?= $(shell cat /proc/cpuinfo | grep "model name" | wc -l)

hn_header := $(MANGO_ROOT)/include/libhn/hn.h
hn_daemon := $(MANGO_ROOT)/bin/hn_daemon

libbbque_pms := $(BOSP_PATH)/lib/bbque/libbbque_pms.so
libbbque_tg  := $(BOSP_PATH)/lib/bbque/libbbque_tg.so
libmango-so  := $(MANGO_ROOT)/lib/libmango.so

PROJECT_NAME  := "MANGO Platform Libraries"
SRCDIR := $(shell pwd)
OUTDIR ?= $(SRCDIR)/build

GIT_VERSION := `git log --oneline -1 | awk '{ print $1 }'`


.PHONY: all libhn libgn libmango bosp toolchains sdk sdk-peak samples


all: bosp libmango samples
	@echo
	@echo "============================================="
	@echo "         .:: $(PROJECT_NAME) ::              "
	@echo "============================================="
	@echo " ProDesign tools .............: " $(CONFIG_PRODESIGN_PATH)
	@echo " Build type ..................: " $(BUILD_TYPE)
	@echo " Build jobs ..................: " $(CPUS)
	@echo " Source directory ............: " $(SRCDIR)
	@echo " Installation directory ......: " $(MANGO_ROOT)
	@echo " BOSP installation directory  : " $(BOSP_PATH)
	@echo " Version (GIT commit)         : " $(GIT_VERSION)
	@echo "============================================="
	@echo

-include Makefile.kconf

-include .config

version:
	@echo "Version: "$(GIT_VERSION)


ifdef CONFIG_MANGO_BUILD_DEBUG
BUILD_TYPE = "Debug"
endif
ifdef CONFIG_MANGO_ROOT_DIR
MANGO_ROOT = $(CONFIG_MANGO_ROOT_DIR)
endif

toolchain_peak := $(MANGO_ROOT)/usr/local/mipsel-unknown-gappeak
toolchain_nup  := $(MANGO_ROOT)/usr/local/llvm-nuplus/


ifdef CONFIG_LIBMANGO_GN   #=== GN emulation =======================================

lib_arch := $(MANGO_ROOT)/lib/libgn.so

sdk:
	@echo
	@echo "----------------------------------------------"
	@echo "                   SDK                        "
	@echo "----------------------------------------------"
	@echo ...nothing to be done for GN Emulation...:
	@echo
else                       #=== GN emulation =====================================

lib_arch := $(MANGO_ROOT)/lib/libhn.so

sdk: toolchains sdk-peak
	@echo
	@echo " SDK installed."
	@echo

sdk-peak:
	@echo
	@echo "----------------------------------------------"
	@echo "           SDK for PEAK processors           "
	@echo "----------------------------------------------"
	@echo
	@cd $(SRCDIR)/sdk-peak/src && \
		MANGO_ROOT=$(MANGO_ROOT) PEAK_COMPILER_PATH=$(toolchain_peak)/bin/ make && \
		sudo MANGO_ROOT=$(MANGO_ROOT) PEAK_COMPILER_PATH=$(toolchain_peak)/bin/ make install


toolchains: toolchain-peak toolchain-nup
	@echo
	@echo " Toolchains installed."
	@echo

toolchain-peak: $(toolchain_peak)

$(toolchain_peak):
	@echo
	@echo "----------------------------------------------"
	@echo "              PEAK toolchain                  "
	@echo "----------------------------------------------"
	@cd $(MANGO_ROOT) && \
		sudo tar -Jxvf $(SRCDIR)/toolchain-gappeak/mipsel-unknown-gappeak.txz

ifdef CONFIG_LIBMANGO_NUPLUS

toolchain-nup: $(toolchain_nup)

$(toolchain_nup): $(SRCDIR)/toolchain-nup
	@echo
	@echo "----------------------------------------------"
	@echo "              NU+ toolchain                  "
	@echo "----------------------------------------------"
	@cd $(SRCDIR)/toolchain-nup/compiler && \
		mkdir -p build/ && cd build/ && \
		cmake .. -DCMAKE_INSTALL_PREFIX=$(toolchain_nup) && \
		make -j$(CPUS) && \
		sudo make install
	@cd $(SRCDIR)/toolchain-nup/ && \
		TOPDIR=$(SRCDIR)/toolchain-nup MANGO_ROOT=$(MANGO_ROOT) make

else

toolchain-nup:
	@echo
	@echo "----------------------------------------------"
	@echo "              NU+ toolchain                  "
	@echo "----------------------------------------------"
	@echo Skipping this toolchain...

endif #  CONFIG_LIBMANGO_NUPLUS

endif #  CONFIG_LIBMANGO_GN 


ifndef CONFIG_LIBMANGO_GN

$(lib_arch): libhn

$(hn_header): libhn $(hn_daemon)

$(hn_daemon): $(SRCDIR)/libhn/hn_daemon
	@echo
	@echo "----------------------------------------------"
	@echo " HN daemon                                    "
	@echo "----------------------------------------------"
	@cd $^ && instdir=$(MANGO_ROOT) prodesign_dir=$(CONFIG_PRODESIGN_PATH) \
		build_type=$(BUILD_TYPE) make autostart_tempmon=yes -j$(CPUS) && \
		sudo instdir=$(MANGO_ROOT) make install

else     # ==== GN emulation =======================================================

$(lib_arch): libgn

$(hn_header): $(SRCDIR)/libhn/
	@echo
	@echo "----------------------------------------------"
	@echo " HN library headers                           "
	@echo "----------------------------------------------"
	@cd $(SRCDIR)/libhn && sudo instdir=$(MANGO_ROOT) make install_headers

$(hn_daemon): $(SRCDIR)/libhn/hn_daemon
	@echo
	@echo "----------------------------------------------"
	@echo " HN daemon headers                            "
	@echo "----------------------------------------------"
	@cd $(SRCDIR)/libhn/hn_daemon && sudo instdir=$(MANGO_ROOT) make install_headers

endif #  CONFIG_LIBMANGO_GN ======================================================


libhn: $(SRCDIR)/libhn/
	@echo
	@echo "----------------------------------------------"
	@echo " HN library                                   "
	@echo "----------------------------------------------"
	@cd $(SRCDIR)/libhn && instdir=$(MANGO_ROOT) \
		build_type=$(BUILD_TYPE) make -j$(CPUS) && \
		sudo instdir=$(MANGO_ROOT) make install


libgn: $(hn_header) $(hn_daemon)
	@echo
	@echo "----------------------------------------------"
	@echo " GN library                                   "
	@echo "----------------------------------------------"
	@export CONFIG_LIBMANGO_GN
	@cd $(SRCDIR)/libgn && mkdir -p build && cd build \
		&& cmake .. -DBOSP_PATH=$(BOSP_PATH) \
			-DMANGO_ROOT=$(MANGO_ROOT) \
			-DBUILD_TYPE=$(BUILD_TYPE) \
		&& make -j$(CPUS) && sudo make install

bosp: $(lib_arch) $(hn_header)
	@echo
	@echo "----------------------------------------------"
	@echo "              BarbequeRTRM                    "
	@echo "----------------------------------------------"
	@cd $(SRCDIR)/bosp && make menuconfig && make -j$(CPUS)

$(SRCDIR)/bosp/out: install_bosp

install_bosp: bosp
	@echo
	@echo "----------------------------------------------"
	@echo "              BarbequeRTRM installation       "
	@echo "----------------------------------------------"
	@if [ ! -d $(BOSP_PATH) ]; then sudo mkdir -p $(BOSP_PATH); fi
	@sudo cp -r $(SRCDIR)/bosp/out/* $(BOSP_PATH)


$(libbbque_pms): $(SRCDIR)/bosp/out

$(libbbque_tg): $(SRCDIR)/bosp/out

$(libmango-so): libmango

libmango: sdk $(lib_arch) $(libbbque_pms) $(libbbque_tg)
	@echo
	@echo "----------------------------------------------"
	@echo "              MANGO API library               "
	@echo "----------------------------------------------"
	@cd $(SRCDIR)/libmango && mkdir -p build && cd build \
		&& cmake .. -DBOSP_PATH=$(BOSP_PATH) \
			-DMANGO_ROOT=$(MANGO_ROOT) \
			-DBUILD_TYPE=$(BUILD_TYPE) \
		&& make -j$(CPUS) && sudo make install


# ==================== Clean targets ===============================

.PHONY: clean clean_libhn clean_libgn clean_bbque clean_libmango clean_sdk-peak clean_hn-daemon

clean_libhn:
	@echo "Cleaning libhn..."
	@cd libhn && make clean

clean_hn-daemon:
	@echo "Cleaning hn-daemon..."
	@cd libhn/hn_daemon && make clean

clean_libgn:
	@echo "Cleaning libgn..."
	@cd libgn && rm -rf build/

clean_bbque:
	@echo "Cleaning BOSP..."
	@cd bosp && make clean_bbque

clean_libmango:
	@echo "Cleaning libmango..."
	@cd libmango && if [ -d "build" ]; then rm -r build; fi

clean_sdk-peak:
	@echo "Cleaning PEAK SDK..."
	@cd $(SRCDIR)/sdk-peak/src && make clean

clean_toolchain-nup:
	@echo "Cleaning NU+ Toolchain..."
	@cd $(SRCDIR)/toolchain-nup/compiler/build &&  make clean && \
		sudo rm -rf $(SRCDIR)/toolchain-nup/compiler/build
	@cd $(SRCDIR)/toolchain-nup/ &&  make clean


clean: clean_libhn clean_hn-daemon clean_libgn clean_bbque clean_libmango clean_sdk-peak clean_samples
	@echo "Cleaning..."
	@if [ -d "build" ]; then rm -r $(OUTDIR); fi
	@echo " Done."


clean_config:
	@echo "Removing configuration files..."
	@rm -f .config $(cmake_config)

distclean: clean clean_config
	@cd $(SRCDIR)/bosp && make clean && make clean_out


# ==================== Samples targets ===============================

samples: # $(libmango-so)
	@echo
	@echo "----------------------------------------------"
	@echo "          MANGO Sample applications           "
	@echo "----------------------------------------------"
	@cd samples && mkdir -p build && cd build \
		&& cmake .. -DBOSP_PATH=$(MANGO_ROOT)/bosp \
			-DMANGO_ROOT=$(MANGO_ROOT) \
			-DBUILD_TYPE=$(BUILD_TYPE) \
		&& make && sudo make install

clean_samples:
	@cd $(SRCDIR)/samples && cd build && make clean
	@cd $(SRCDIR)/samples && rm -r build
	@cd $(SRCDIR)/samples/nuplus_matrix_multiplication/kernel && rm -r obj/

